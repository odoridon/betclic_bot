import sqlite3


class Betabase:

    def __init__(self):
        self.cursor = None
        self.table_name = None
        self.database = None

    def create_database(self, database_name: str):
        database_name = database_name.lower()
        self.database = sqlite3.connect(database_name)
        self.cursor = self.database.cursor()

    def create_table(self, table_name: str):
        self.table_name = table_name.lower()
        self.cursor.execute(f"CREATE TABLE IF NOT EXISTS {table_name} (datetime string, teams string, score string, "
                            f"bet_values string, minute integer, bet_team string, stake integer, status string)")
        self.database.commit()

    def insert_values(self, bet_data: dict):
        betting_time = bet_data['betting_time']
        teams_data = bet_data['teams_data']
        stake = bet_data['stake']

        for event in teams_data:
            first_team_name = event['first_team']['team_name']
            second_team_name = event['second_team']['team_name']
            first_team_score = event['first_team']['score']
            second_team_score = event['second_team']['score']
            first_team_bet = event['first_team']['bet'].replace(',', '.')
            draw_bet = event['draw']['bet'].replace(',', '.')
            second_team_bet = event['second_team']['bet'].replace(',', '.')
            minute = event['minute']
            bet_team = event['bet_team']
            status = event['status']
            tuple_data = (
                betting_time, f'{first_team_name}, {second_team_name}', f'{first_team_score}, {second_team_score}',
                f'{first_team_bet}, {draw_bet}, {second_team_bet}', minute, bet_team, stake, status)
            self.cursor.execute(f"INSERT INTO {self.table_name} VALUES (?, ?, ?, ?, ?, ?, ?, ?)", tuple_data)
            self.database.commit()

    def close_database(self):
        self.database.close()
