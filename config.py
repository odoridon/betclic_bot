account_data = {
    'email': '@gmail.com',
    'password': '',
    'birthdate': ''  # d/m/y
}
page_elements = {
    'login_page': 'https://www.betclic.pl/logowanie?returnUrl=%2F',
    'privacy_button': '//*[@id="popin_tc_privacy_button_3"]',
    'login_input': '/html/body/app-desktop/div[1]/div/bcdk-content-scroller/div/bc-login-page/div/div/div[2]/div['
                   '1]/div[2]/bc-login-form/form/div[1]/bcdk-input-base/input',
    'password_input': '/html/body/app-desktop/div[1]/div/bcdk-content-scroller/div/bc-login-page/div/div/div[2]/div['
                      '1]/div[2]/bc-login-form/form/div[2]/bcdk-input-password/input',
    'login_button': '/html/body/app-desktop/div[1]/div/bcdk-content-scroller/div/bc-login-page/div/div/div[2]/div['
                    '1]/div[3]/div/button[1]',
    'birthdate_input': '/html/body/app-desktop/div[1]/div/bcdk-content-scroller/div/bc-digests-page/div/div/div/div'
                       '/bc-digest-birthdate/div/div[2]/div/bcdk-birthdate-input/bcdk-date-input/input',
    'birthdate_pass_button': '/html/body/app-desktop/div[1]/div/bcdk-content-scroller/div/bc-digests-page/div/div/div'
                             '/div/div[3]/bc-digests-footer/div/button[1]',
    'last_winnings_page': 'https://www.betclic.pl/winnings',
    'close_last_winnings_button': '/html/body/app-desktop/div[1]/div/bcdk-content-scroller/div/winnings-page/div/div'
                                  '/div[3]/button',
    'live_bets_page': 'https://www.betclic.pl/live',
    'stake_input': 'stakeField',
    'bet_button': 'betBtn',
    'bet_done_element': '/html/body/div/div[2]/div/mat-dialog-container',
    'basket_button': '/html/body/app-desktop/div[1]/div/div/div/sports-right-menu/sports-betting_classes-slip/div/div'
                     '/betting_classes-slip-desktop-header/div/div[2]/div',
    'my_bets_page': 'https://www.betclic.pl/my-bets',
    'ended_bets_button': 'ended',
    'scroll_down_bet_button': '/html/body/app-desktop/div[1]/div/bcdk-content-scroller/div/my-bets-page/div/my-bets'
                              '-sports-list/bet-card[1]/div/bet-card-header/div/div[1]/bet-card-status-list/div/div[2]',
}
