import time
from datetime import datetime
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
import config


class Betclic:

    def __init__(self):
        self.driver = None
        self.wait = None
        self.actions = None
        self.elements = config.page_elements

    def configure_webdriver(self):
        self.driver = webdriver.Chrome()
        self.driver.maximize_window()
        self.driver.implicitly_wait(3)
        self.wait = WebDriverWait(self.driver, 10)
        self.actions = ActionChains(self.driver)

    def _click_by_xpath(self, xpath: str):
        self.wait.until(EC.element_to_be_clickable((By.XPATH, xpath))).click()

    def _send_keys_by_xpath(self, xpath: str, keys: str):
        self.wait.until(EC.presence_of_element_located((By.XPATH, xpath))).send_keys(keys)

    def login(self, email: str, password: str, birthdate: str):
        self.driver.get(self.elements['login_page'])

        self._click_by_xpath(self.elements['privacy_button'])

        # entering login (email)
        self._send_keys_by_xpath(self.elements['login_input'], email)

        # entering password
        self._send_keys_by_xpath(self.elements['password_input'], password)

        # clicking login button
        self._click_by_xpath(self.elements['login_button'])

        # entering date of birth
        self._send_keys_by_xpath(self.elements['birthdate_input'], birthdate)

        # clicking button for passing the data of birth
        self._click_by_xpath(self.elements['birthdate_pass_button'])

        # checking if opens url with last winnings notifications (if yes, pressing ok)
        if self.driver.current_url == self.elements['last_winnings_page']:
            self._click_by_xpath(self.elements['close_last_winnings_button'])

    def go_live_games_page(self):
        # scrolling down page to load html code
        self.driver.get(self.elements['live_bets_page'])
        waiting_time = 3
        for i in range(3):
            self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
            time.sleep(waiting_time)

    def extract_football_data(self):
        soup = BeautifulSoup(self.driver.page_source, 'html.parser')

        # creating list for storing current bets data
        football_data = []

        # scraping football bets data
        football_element = soup.find('sports-live-event-bucket')
        football_events = football_element.div.find('div', class_='accordionList_lvl2').find_all('sports-events-event')

        for event in football_events:
            # checking if betting_classes  is possible at current moment
            if event.find(class_='event_alertWrapper') is not None or event.find(
                    class_='oddValue').getText().strip() == 'Postaw teraz' or 'is-suspended' in \
                    event.find('sports-selections-selection')['class']:
                continue

            values = [value.getText().strip() for value in event.find_all(class_='oddValue')]
            if '-' in values:
                continue

            # team names
            team_names = event.find_all(class_='scoreboard_contestantLabel')
            first_team_name = team_names[0].getText().strip()
            second_team_name = team_names[1].getText().strip()

            # bet values
            team_bets = event.find_all(class_='oddValue')
            first_team_bet = team_bets[0].getText().strip()
            draw_bet = team_bets[1].getText().strip()
            second_team_bet = team_bets[2].getText().strip()

            # button classes
            buttons = event.find_all('sports-selections-selection')
            first_team_button_class = buttons[0]['class']
            first_team_button_class = ', '.join(first_team_button_class)

            draw_button_class = buttons[1]['class']
            draw_button_class = ', '.join(draw_button_class)

            second_team_button_class = buttons[2]['class']
            second_team_button_class = ', '.join(second_team_button_class)

            # time status
            time_status = event.find('scoreboards-timer').getText().strip()
            minute = ''
            if time_status[0].isdigit():
                for character in time_status:
                    if character.isdigit():
                        minute += character
                    else:
                        minute = int(minute)
                        break
            elif time_status == 'Poł.':
                minute = 45
            else:
                minute = 0

            # score
            score = event.find_all(class_='scoreboard_score')
            first_team_score = int(score[0].getText().strip())
            second_team_score = int(score[1].getText().strip())
            score_difference = abs(first_team_score - second_team_score)

            # creating data dictionaries
            first_team_data = {
                'team_name': first_team_name,
                'bet': first_team_bet,
                'button_class': first_team_button_class,
                'score': first_team_score
            }
            draw_data = {
                'team_name': 'DRAW',
                'bet': draw_bet,
                'button_class': draw_button_class
            }
            second_team_data = {
                'team_name': second_team_name,
                'bet': second_team_bet,
                'button_class': second_team_button_class,
                'score': second_team_score
            }

            football_data.append([first_team_data, draw_data, second_team_data, minute, score_difference])

        return football_data

    def make_bet(self, football_data: list, max_single_ticket_value: float, max_tickets_amount: int,
                 min_score_difference: int, min_minute: int, stake: int):
        ticket_amount = 0
        current_datetime = datetime.now().strftime('%d/%m/%Y')
        bet_events = {'betting_time': current_datetime, 'teams_data': [], 'stake': stake}

        # checking data
        for game in football_data:
            # ticket amount
            if ticket_amount == max_tickets_amount:
                break

            # minimum bet value
            bet_values = [float(data['bet'].replace(',', '.')) for data in game[:3]]
            min_value_index = bet_values.index(min(bet_values))
            min_value = bet_values[min_value_index]
            if min_value > max_single_ticket_value:
                continue

            # score difference
            if game[4] < min_score_difference:
                continue

            # minute
            if game[3] < min_minute:
                continue

            # clicking team betting_classes button
            button_to_bet = game[min_value_index]['button_class']
            element = self.driver.find_element(By.CLASS_NAME, button_to_bet)
            self.actions.move_to_element(element).perform()
            self.wait.until(EC.element_to_be_clickable((By.CLASS_NAME, button_to_bet))).click()

            ticket_amount += 1
            bet_team = game[min_value_index]['team_name']
            game.append(bet_team)
            bet_events['teams_data'].append(game)

        # clicking final betting_classes button
        try:
            self.wait.until(EC.element_to_be_clickable((By.NAME, self.elements['stake_input']))).send_keys(f'{stake}')
            self.wait.until(EC.element_to_be_clickable((By.ID, self.elements['bet_button']))).click()
            self.wait.until(EC.presence_of_element_located((By.XPATH, self.elements['bet_done_element'])))
            return bet_events
        except:
            if len(bet_events['teams_data']) > 0:
                self._click_by_xpath(self.elements['basket_button'])
            return None

    def get_last_bet_info(self):
        # going ended bets page
        self.driver.get(self.elements['my_bets_page'])
        self.wait.until(EC.element_to_be_clickable((By.ID, self.elements['ended_bets_button']))).click()
        waiting_time = 3
        time.sleep(waiting_time)

        soup = BeautifulSoup(self.driver.page_source, 'html.parser')
        ticket_type = soup.find('bet-card').find(class_='betCard_headerTitle').getText().strip()

        # checking bet type
        if ticket_type == 'Pojedynczy':
            pass
        else:
            self.wait.until(EC.element_to_be_clickable((By.XPATH, self.elements['scroll_down_bet_button']))).click()
            self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight)")
            waiting_time = 3
            time.sleep(waiting_time)

        # scraping data
        bets_info = []
        soup = BeautifulSoup(self.driver.page_source, 'html.parser')
        last_bets = soup.find('bet-card').find_all('bet-card-event-market')
        for bet in last_bets:
            team_names = bet.find_all(class_='scoreboard_contestantLabel')
            first_team_name = team_names[0].getText().strip()
            second_team_name = team_names[1].getText().strip()
            value = bet.find(class_='oddValue').getText().strip()

            # checking if team bet is won
            status_element = bet.find(class_='marketBets_value')['class']
            if 'is-won' in status_element:
                status = 'WIN'
            else:
                status = 'LOST'

            bets_info.append({'team_names': [first_team_name, second_team_name], 'status': status, 'value': value})

        return bets_info

    def complete_data(self, last_bet: dict, last_bet_info: list):
        # completing data
        complete_data = {'betting_time': last_bet['betting_time'], 'teams_data': [], 'stake': last_bet['stake']}
        for bet_info in last_bet_info:
            for bet in last_bet['teams_data']:
                if bet_info['team_names'][0] == bet[0]['team_name']:
                    data_to_append = {
                        'first_team': bet[0],
                        'draw': bet[1],
                        'second_team': bet[2],
                        'minute': bet[3],
                        'bet_team': bet[5],
                        'status': bet_info['status']
                    }
                    last_bet['teams_data'].remove(bet)
                    complete_data['teams_data'].append(data_to_append)
                    break
        return complete_data

    def quit_browser(self):
        waiting_time = 3
        time.sleep(waiting_time)
        self.driver.quit()
