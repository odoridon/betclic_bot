import time
from betting.betting_class import Betclic
from database.bets_database_class import Betabase
from visualizing.visualizing_data_class import Betualize
import config

# unpacking account data
account_data = config.account_data
email = account_data['email']
password = account_data['password']
birthdate = account_data['birthdate']

while True:
    betclic = Betclic()
    database = Betabase()
    betualize = Betualize()
    database.create_database('database/bets.db')
    database.create_table('events')

    # trying to make bet until it's done
    while True:
        betclic.configure_webdriver()
        betclic.login(email, password, birthdate)
        betclic.go_live_games_page()
        football_data = betclic.extract_football_data()
        bet_events = betclic.make_bet(football_data, 1.20, 5, 0, 40, 1)

        # waiting before trying again
        if bet_events is None:
            waiting_time = 300
            time.sleep(waiting_time)
            continue
        else:
            betclic.quit_browser()
            break

    # waiting 3 hours for event to finish (football game lasts 90 minutes)
    waiting_time = 10800
    time.sleep(waiting_time)

    # scraping last bet info
    betclic.configure_webdriver()
    betclic.login(email, password, birthdate)
    last_bet_info = betclic.get_last_bet_info()
    complete_data = betclic.complete_data(bet_events, last_bet_info)
    betclic.quit_browser()

    # inserting last bet data into database
    database.insert_values(complete_data)
    database.close_database()

    # visualizing data
    betualize.connect_database('database/bets.db')
    start_date = {'year': 2023, 'month': 10, 'day': 15}
    end_date = {'year': 2023, 'month': 12, 'day': 31}
    data_to_visualize = betualize.data_to_visualize(start_date, end_date, 'events')
    betualize.visualize_data(data_to_visualize, 'win')
    betualize.visualize_data(data_to_visualize, 'lost')

    # waiting a day to rerun code
    time.sleep(86400)
