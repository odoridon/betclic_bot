import sqlite3
from datetime import date
from dateutil.rrule import rrule, DAILY
import matplotlib.pyplot as plt


class Betualize:

    def __init__(self):
        self.database = None
        self.cursor = None

    def connect_database(self, database_name: str):
        self.database = sqlite3.connect(database_name)
        self.cursor = self.database.cursor()

    def data_to_visualize(self, start_date: dict, end_date: dict, table_name: str):
        start_date = date(start_date['year'], start_date['month'], start_date['day'])
        end_date = date(end_date['year'], end_date['month'], end_date['day'])

        # retrieving data
        data_to_visualize = []
        for day in rrule(DAILY, dtstart=start_date, until=end_date):
            date_to_select = day.strftime('%d/%m/%Y')
            self.cursor.execute(f"SELECT * FROM {table_name} WHERE datetime = '{date_to_select}'")
            betting_tickets = self.cursor.fetchall()
            if len(betting_tickets) != 0:
                status = [ticket[7] for ticket in betting_tickets]
                stake = betting_tickets[0][6]

                if 'LOST' in status:
                    data = {'date': date_to_select, 'win': 0, 'lost': stake}
                    data_to_visualize.append(data)
                    continue
                else:
                    all_bet_values = []
                    for ticket in betting_tickets:
                        teams = ticket[1]
                        teams = teams.split(', ')
                        bet_team = ticket[5]

                        if bet_team != 'DRAW':
                            index = teams.index(bet_team)
                            if index != 0:
                                index = 2
                        else:
                            index = 1

                        bet_values = ticket[3]
                        bet_values = bet_values.split(', ')
                        bet_value = bet_values[index]
                        all_bet_values.append(bet_value)

                    final_bet_value = float(all_bet_values[0])
                    for bet_value in all_bet_values[1:]:
                        bet_value = float(bet_value)
                        final_bet_value *= bet_value

                    final_bet_value = final_bet_value - final_bet_value / 100 * 12
                    win = round(final_bet_value * stake, 2)
                    data = {'date': date_to_select, 'win': win, 'lost': 0}
                    data_to_visualize.append(data)

        self.database.close()

        return data_to_visualize

    def visualize_data(self, data_to_visualize: list, status: str):
        if status == 'win':
            dates = []
            wins = []
            for data in data_to_visualize:
                date = data['date']
                dates.append(date)
                win = data['win']
                wins.append(win)

            plt.bar(dates, wins, color='turquoise', width=0.2)
            plt.xlabel('Date')
            plt.ylabel('Win Amount')
            plt.title('Betting Wins Over Time')

            plt.show()
        else:
            dates = []
            wins = []
            for data in data_to_visualize:
                date = data['date']
                dates.append(date)
                win = data['lost']
                wins.append(win)

            plt.bar(dates, wins, color='salmon', width=0.2)
            plt.xlabel('Date')
            plt.ylabel('Win Amount')
            plt.title('Betting Losings Over Time')

            plt.show()
